name := "KafkaTraining"

version := "0.1"

scalaVersion := "2.12.4"

resolvers += Resolver.bintrayRepo("cakesolutions", "maven")

resolvers += "clojars" at "https://clojars.org/repo"

resolvers += "confluentmaven" at "http://packages.confluent.io/maven/"

libraryDependencies ++= Seq(
    "org.apache.kafka"           % "kafka-clients"         % "1.0.0",
    "net.cakesolutions"          %% "scala-kafka-client"   % "1.0.0",
    "com.typesafe"               % "config"                % "1.2.1",
    "kafka-avro-confluent"       % "kafka-avro-confluent"  % "0.1.0",
    "io.confluent"               % "kafka-schema-registry" % "3.3.0",
    "io.confluent"               % "kafka-avro-serializer" % "3.3.0",
    "com.typesafe.scala-logging" %% "scala-logging"        % "3.8.0",
    "org.specs2"                 %% "specs2-core"          % "4.0.3" % Test
)
