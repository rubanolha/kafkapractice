package utils

import java.io.Closeable

trait CloseableResource {

  def using[T <: Closeable, B](resource: T)(block: T => B) = {
    try {
      block(resource)
    } finally {
      if (resource != null) resource.close()
    }
  }

}