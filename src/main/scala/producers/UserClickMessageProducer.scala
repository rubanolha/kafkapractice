package producers

import AvroModified.avro.UserClick
import cakesolutions.kafka.KafkaProducer
import org.apache.kafka.clients.producer.ProducerRecord

case class UserClickMessageProducer(producerTopic: String, producer: KafkaProducer[Long, UserClick])
  extends KafkaMessageProducer[Long, UserClick] {

 override def toProducerRecord(click: UserClick): ProducerRecord[Long, UserClick] = {
    new ProducerRecord[Long, UserClick](
      producerTopic,
      click.getSiteId,
      click
    )
  }

}

object UserClickGenerator {
  private val idGenerator = Iterator.from(0)
  val anyLongVal = 100l

  def next(): UserClick = {
    new UserClick(idGenerator.next().toString, anyLongVal, anyLongVal, anyLongVal, anyLongVal)
  }

}
