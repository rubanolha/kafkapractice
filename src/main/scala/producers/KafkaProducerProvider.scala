package producers

import java.io.Closeable

import cakesolutions.kafka.KafkaProducer
import com.typesafe.config.Config
import configUtil.ConfigUtil.toProperties
import org.apache.kafka.clients.producer.{KafkaProducer => JKafkaProducer}

case class KafkaProducerProvider[K, V](config: Config) extends Closeable {

  lazy val producer = KafkaProducer(new JKafkaProducer[K, V](toProperties(config)))

  override def close(): Unit = {
    producer.flush()
    producer.close()
  }
}


