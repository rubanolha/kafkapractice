package producers

import java.util.UUID

import cakesolutions.kafka.KafkaProducer
import org.apache.kafka.clients.producer.ProducerRecord

case class RandomStringProducer(producerTopic: String, producer: KafkaProducer[String, String])
    extends KafkaMessageProducer[String, String] {

  override def toProducerRecord(value: String): ProducerRecord[String, String] =
    new ProducerRecord(producerTopic, value)
}

object RandomStringGenerator {
  def nextStr(): String = UUID.randomUUID().toString
}
