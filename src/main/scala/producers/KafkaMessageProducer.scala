package producers

import cakesolutions.kafka.KafkaProducer
import com.typesafe.scalalogging.LazyLogging
import org.apache.kafka.clients.producer.{ProducerRecord, RecordMetadata}

import scala.concurrent.Future
import scala.util.{Failure, Success, Try}
import scala.concurrent.ExecutionContext.Implicits.global

trait KafkaMessageProducer[K, V] extends LazyLogging {

  val producer: KafkaProducer[K, V]

  def toProducerRecord(value: V): ProducerRecord[K, V]

  def process(value: V): Future[RecordMetadata] = {
    val future = producer.send(toProducerRecord(value))
    future.onComplete(logRecordMetadataResult)
    future
  }

  def logRecordMetadataResult(result: Try[RecordMetadata]): Unit = result match {
    case Failure(e) => logger.error(e.getMessage)
    case Success(m) => logger.info(s"Message was sent to topic ${m.topic()}, timestamp ${m.timestamp()}")
  }

}