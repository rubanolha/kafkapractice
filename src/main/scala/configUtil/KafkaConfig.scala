package configUtil

import com.typesafe.config.Config
import configUtil.Requires.requireNonEmpty

case class KafkaConfig(conf: Config) {

  lazy val topic: String = conf.getString("topic")

  val mandotaryConfigList = List("topic", "bootstrap.servers", "key.serializer", "value.serializer")

  mandotaryConfigList.foreach(pr => requireNonEmpty(conf, pr))

}
