package configUtil

import java.util.Properties

import com.typesafe.config.{Config, ConfigFactory}

object Requires {

  def requireNonEmpty(conf: Config, path: String): Unit = {
    require(conf.hasPath(path), s"$path must be not null")
    require(!conf.getString(path).trim.isEmpty, s"$path must be not empty")
  }

}

object ConfigUtil {

  import scala.collection.JavaConverters._

  def loadFromEnvironment: Config = {
    ConfigFactory.load()
  }

  def loadFromEnvironmentPath(path: String): Config = {
    loadFromEnvironment.getConfig(path)
  }

  def toProperties(config: com.typesafe.config.Config): Properties = {
    val props = new Properties()
    config
      .entrySet()
      .asScala
      .foreach(entry => props.put(entry.getKey, entry.getValue.unwrapped().toString))
    props
  }
}
