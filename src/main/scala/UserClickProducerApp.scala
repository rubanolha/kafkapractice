import AvroModified.avro.UserClick
import configUtil.ConfigUtil._
import configUtil.KafkaConfig
import producers.{KafkaProducerProvider, UserClickGenerator, UserClickMessageProducer}
import utils.CloseableResource

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.concurrent.ExecutionContext.Implicits.global

object UserClickProducerApp extends App with CloseableResource {

  lazy val kafkaConfig = KafkaConfig(loadFromEnvironmentPath("userClickProducer"))

  using(KafkaProducerProvider[Long, UserClick](kafkaConfig.conf)) { kafkaProducer =>
    val messageProducer = UserClickMessageProducer(kafkaConfig.topic, kafkaProducer.producer)
    val messagesFutures = (1 to 5).map(_ => { messageProducer.process(UserClickGenerator.next()) })
    Await.ready(Future.sequence(messagesFutures), 1 minutes)
    //  Wait for onComplete to be logged
    Thread.sleep(1000)
  }

}
