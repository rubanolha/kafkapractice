import configUtil.ConfigUtil.loadFromEnvironmentPath
import configUtil.KafkaConfig
import producers.{KafkaProducerProvider, RandomStringGenerator, RandomStringProducer}
import utils.CloseableResource

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

object RandomStringProducerApp extends App with CloseableResource {

  lazy val kafkaConfig = KafkaConfig(loadFromEnvironmentPath("stringProducer"))

  using(KafkaProducerProvider[String, String](kafkaConfig.conf)) { kafkaProducer =>
    val messageProducer = RandomStringProducer(kafkaConfig.topic, kafkaProducer.producer)
    val messagesFutures = (1 to 10).map(_ => messageProducer.process(RandomStringGenerator.nextStr()))
    Await.ready(Future.sequence(messagesFutures), 1 minute)
    //  Wait for onComplete to be logged
    Thread.sleep(1000)
  }

}
