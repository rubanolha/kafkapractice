package producers

import AvroModified.avro.UserClick
import org.specs2.mutable.Specification

class UserClickGeneratorSpec extends Specification {

  sequential

  "A UserClickGenerator" should {

    "produce UserClick object starting from UserId = 0" in {
      val expectedUserClick = new UserClick("0", 100l, 100l, 100l, 100l)
      UserClickGenerator.next() mustEqual expectedUserClick
    }

    "increase UserId with every next() call" in {
      val expectedUserClick = new UserClick("1", 100l, 100l, 100l, 100l)
      UserClickGenerator.next() mustEqual expectedUserClick
    }

  }

}
