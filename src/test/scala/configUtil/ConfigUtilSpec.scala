package configUtil

import com.typesafe.config.ConfigException.Missing
import org.specs2.mutable.Specification
import configUtil.ConfigUtil._

class ConfigUtilSpec {}

class KafkaConfigSpec extends Specification {

  "A KafkaConfig" should {

    def getKafkaConfig(path: String): KafkaConfig = KafkaConfig(loadFromEnvironmentPath(path))

    "produce an exception if config path not found" in {
      val path = "nonexistentConfigPah"
      getKafkaConfig(path) should throwAn[Missing]
    }

    "produce an exception if mandatory property is not declared in config" in {
      val configPath = "testConfigNoMandatoryValue"
      getKafkaConfig(configPath) should throwAn[IllegalArgumentException]
    }

    "produce an exception if mandatory property value is empty" in {
      val path = "testConfigEmptyValue"
      getKafkaConfig(path) should throwAn[IllegalArgumentException]
    }

    "return config with all declared fields" in {
      val path   = "validConfig"
      val config = getKafkaConfig(path).conf
      config.getString("topic") must beEqualTo("test.topic")
      config.getString("bootstrap.servers") must beEqualTo("testhost")
      config.getString("key.serializer") must beEqualTo("test.key.serializer")
      config.getString("value.serializer") must beEqualTo("test.value.serializer")
      config.getString("acks") must beEqualTo("1")
    }

    "return topic value from config" in {
      val path   = "validConfig"
      val config = getKafkaConfig(path)
      config.topic mustEqual "test.topic"
    }

  }

}