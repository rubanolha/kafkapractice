# README #

### Overview ###

This is a small example of using Kafka Producer to send messages with different configurations to Kafka Server. It imports Cakesolutions scala-kafka-client library for convenient Scala compatibility. It also produce Avro classes to Kafka using Confluent's Schema Repository and Avro Serializers. 

### Quickstart ###

This example assumes that Zookeeper, Kafka and Schema Registry are started with the default settings.

Start Zookeeper
`$ bin/zookeeper-server-start config/zookeeper.properties`
Start Kafka
`$ bin/kafka-server-start config/server.properties`
Start Schema Registry
` $ bin/schema-registry-start config/schema-registry.properties `

To run the UserClickProducer producer
`sbt "runMain UserClickProducerApp"`

To run the RandomStringProducer producer
`sbt "runMain UserClickProducerApp"`

To test
`sbt test`

### Contact ###

* Repo owner - rubanolha@gmail.com